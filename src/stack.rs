pub trait Stack<T: Clone> {
    fn collect(&self) -> Vec<T>;
    fn push<'a>(&'a self, elem: T) -> StackElem<'a, T>;
}

pub struct StackBase;
impl<T: Clone> Stack<T> for StackBase {
    fn collect(&self) -> Vec<T> {
        Vec::new()
    }
    fn push<'a>(&'a self, elem: T) -> StackElem<'a, T> {
        StackElem{pred: self, elem}
    }
}

pub struct StackElem<'a, T: Clone> {
    pred: &'a dyn Stack<T>,
    elem: T,
}
impl<'a, T: Clone> Stack<T> for StackElem<'a, T> {
    fn collect(&self) -> Vec<T> {
        let mut pred = self.pred.collect();
        pred.push(self.elem.clone());
        pred
    }
    fn push<'b>(&'b self, elem: T) -> StackElem<'b, T> {
        StackElem{pred: self, elem}
    }
}
