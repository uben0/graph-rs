use std::ops::Index;
use std::ops::IndexMut;

pub struct Drawer<T> {
    inner: Vec<Option<T>>,
}

impl<T> Drawer<T> {
    pub fn new() -> Self {
        Self::default()
    }
    pub fn with_capacity(capacity: usize) -> Self {
        Self{ inner: Vec::with_capacity(capacity) }
    }
    pub fn insert(&mut self, index: usize, value: T) -> Option<T> {
        if index < self.inner.len() {
            self.inner[index].replace(value)
        }
        else {
            self.inner.resize_with(index, || None);
            self.inner.push(Some(value));
            None
        }
    }
    pub fn get(&self, index: usize) -> Option<&T> {
        self.inner.get(index)?.as_ref()
    }
    pub fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        self.inner.get_mut(index)?.as_mut()
    }
    pub fn remove(&mut self, index: usize) -> Option<T> {
        if index < self.inner.len() {
            self.inner.remove(index)
        }
        else {
            None
        }
    }
}

impl<T> Default for Drawer<T> {
    fn default() -> Self {
        Self{
            inner: Vec::new(),
        }
    }
}

impl<T> Index<usize> for Drawer<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        self.get(index).expect("index not matching a valid emplacement")
    }
}
impl<T> IndexMut<usize> for Drawer<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        self.get_mut(index).expect("index not matching a valid emplacement")
    }
}