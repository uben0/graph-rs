use std::fmt::Display;
use std::ops::Index;
mod drawer;
pub use drawer::Drawer;
mod stack;
pub use stack::*;

#[derive(Debug, Clone)]
pub struct Graph<T, U> {
    nodes: Vec<Option<T>>,
    matrix: Vec<Vec<U>>,
}
impl<T, U> Default for Graph<T, U> {
    fn default() -> Self {
        Self{
            nodes: Vec::new(),
            matrix: Vec::new(),
        }
    }
}
impl<T, U> Graph<T, U> {
    pub fn new() -> Self {
        Self::default()
    }
    pub fn with_capacity(capacity: usize) -> Self {
        Self{
            nodes: Vec::with_capacity(capacity),
            matrix: Vec::with_capacity(capacity * capacity),
        }
    }
    pub fn has_node(&self, node: usize) -> bool {
        self.nodes.get(node).map(|o| o.is_some()).unwrap_or(false)
    }
    pub fn node_get(&self, node: usize) -> Option<&T> {
        self.nodes.get(node)?.as_ref()
    }
    pub fn node_get_mut(&mut self, node: usize) -> Option<&mut T> {
        self.nodes.get_mut(node)?.as_mut()
    }
    pub fn arcs_get(&self, src: usize, dst: usize) -> Option<&[U]> {
        if self.has_node(src) && self.has_node(dst) {
            Some(self.matrix[src * self.nodes.len() + dst].as_slice())
        }
        else {
            None
        }
    }
    pub fn arcs_get_mut(&mut self, src: usize, dst: usize) -> Option<&mut Vec<U>> {
        if self.has_node(src) && self.has_node(dst) {
            Some(&mut self.matrix[src * self.nodes.len() + dst])
        }
        else {
            None
        }
    }

    pub fn new_node(&mut self, value: T) -> usize {
        let node = self.nodes
            .iter()
            .position(|o| o.is_none())
            .unwrap_or(self.reserve());
        self.nodes[node] = Some(value);
        node
    }
    fn reserve(&mut self) -> usize {
        let old_cap = self.nodes.len();
        let new_cap = if old_cap == 0 {8} else {old_cap * 2};
        self.matrix.resize_with(new_cap * new_cap, || Vec::new());
        self.nodes.resize_with(new_cap, || None);
        for i in (0..old_cap).rev() {
            for j in (0..old_cap).rev() {
                let old_index = i * old_cap + j;
                let new_index = i * new_cap + j;
                self.matrix.swap(old_index, new_index);
            }
        }
        old_cap
    }
    pub fn del_node(&mut self, node: usize) -> Option<T> {
        let value = self.nodes.get_mut(node)?.take()?;
        for i in 0..self.nodes.len() {
            if self.nodes[i].is_some() {
                self.arcs_get_mut(node, i).unwrap().clear();
                self.arcs_get_mut(i, node).unwrap().clear();
            }
        }
        Some(value)
    }

    pub fn new_arc(&mut self, src: usize, dst: usize, value: U) -> Option<usize> {
        let arcs = self.arcs_get_mut(src, dst)?;
        let index = arcs.len();
        arcs.push(value);
        Some(index)
    }

    pub fn nodes(&self) -> NodeIter<T> {
        NodeIter(self.nodes.iter().enumerate())
    }

    pub fn arcs(&self) -> ArcIter<T, U> {
        ArcIter{graph: self, src: 0, dst: 0}
    }


    pub fn node_successors(&self, node: usize) -> SuccIter<T, U> {
        SuccIter{
            arcs: self.matrix[node * self.nodes.len() .. (node + 1) * self.nodes.len()].iter(),
            nodes: self.nodes.iter().enumerate(),
        }
    }
    pub fn node_predecessors(&self, node: usize) -> PredIter<T, U> {
        PredIter{
            arcs: self.matrix[node ..].iter().step_by(self.nodes.len()),
            nodes: self.nodes.iter().enumerate(),
        }
    }
    pub fn map_nodes_index<V, X: Fn(usize)->V>(mut self, node_map: X) -> Graph<V, U> {
        Graph {
            nodes: self.nodes.drain(..).enumerate().map(|(i, o)| o.map(|_| node_map(i))).collect(),
            matrix: self.matrix,
        }
    }

    pub fn map_nodes<V, X: Fn(T)->V>(mut self, node_map: X) -> Graph<V, U> {
        Graph {
            nodes: self.nodes.drain(..).map(|o| o.map(&node_map)).collect(),
            matrix: self.matrix,
        }
    }
    pub fn map_arcs<W, Y: Fn(U)->W>(mut self, arc_map: Y) -> Graph<T, W> {
        Graph {
            nodes: self.nodes,
            matrix: self.matrix.drain(..).map(|mut v| v.drain(..).map(&arc_map).collect()).collect(),
        }
    }

    pub fn map<V, W, X, Y>(&self, mut node_map: X, mut arc_map: Y) -> Graph<V, W>
    where
        X: FnMut(usize, &T)->V,
        Y: FnMut((usize, usize, usize), &U)->W,
    {
        let cap = self.nodes.len();
        let nodes = self.nodes.iter().enumerate().map(|(i, v)| Some(node_map(i, v.as_ref()?))).collect();
        let mut arcs = Vec::with_capacity(cap * cap);
        for src in 0..cap {
            for dst in 0..cap {
                arcs.push(
                    self.matrix[src * cap + dst].iter().enumerate().map(|(i, v)| arc_map((src, dst, i), v)).collect()
                )
            }    
        }
        Graph{
            nodes,
            matrix: arcs,
        }
    }
}

impl<T: Display, U: Display> Graph<T, U> {
    pub fn to_dot(&self, mut output: impl std::io::Write) -> std::io::Result<()> {
        writeln!(output, "digraph 0 {{")?;
        for node in self.nodes() {
            writeln!(output, "  {} [label=\"{}\"];", node, self.node_get(node).unwrap())?;
        }
        for (src, dst) in self.arcs() {
            for arc in self.arcs_get(src, dst).unwrap() {
                writeln!(output, "  {} -> {} [label=\"{}\"];", src, dst, arc)?;
            }
        }
        writeln!(output, "}}")?;
        Ok(())
    }
}

impl<T, U> Index<usize> for Graph<T, U> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        self.node_get(index).expect("node index does not match an existing node")
    }
}
impl<T, U> Index<(usize, usize)> for Graph<T, U> {
    type Output = [U];
    fn index(&self, (src, dst): (usize, usize)) -> &Self::Output {
        self.arcs_get(src, dst).expect("node index does not match an existing node")
    }
}

pub struct NodeIter<'a, T>(std::iter::Enumerate<std::slice::Iter<'a, Option<T>>>);
impl<'a, T> Iterator for NodeIter<'a, T> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        (&mut self.0).filter_map(|(i, o)| {o.as_ref()?; Some(i)}).next()
    }
}

pub struct ArcIter<'a, T, U> {
    graph: &'a Graph<T, U>,
    src: usize,
    dst: usize,
}
impl<'a, T, U> Iterator for ArcIter<'a, T, U> {
    type Item = (usize, usize);
    fn next(&mut self) -> Option<Self::Item> {
        while self.src < self.graph.nodes.len() {
            for dst in self.dst..self.graph.nodes.len() {
                if let Some(arcs) = self.graph.arcs_get(self.src, dst) {
                    if arcs.len() > 0 {
                        self.dst = dst + 1;
                        return Some((self.src, dst));
                    }
                }
            }
            self.dst = 0;
            self.src += 1;
        }
        None
    }
}

pub struct SuccIter<'a, T, U> {
    nodes: std::iter::Enumerate<std::slice::Iter<'a, Option<T>>>,
    arcs: std::slice::Iter<'a, Vec<U>>,
}
impl<'a, T, U> Iterator for SuccIter<'a, T, U> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        (&mut self.arcs).zip(&mut self.nodes).filter(|(a,_)| a.len() > 0).map(|(_,(i, _))| i).next()
    }
}

pub struct PredIter<'a, T, U> {
    nodes: std::iter::Enumerate<std::slice::Iter<'a, Option<T>>>,
    arcs: std::iter::StepBy<std::slice::Iter<'a, Vec<U>>>,
}
impl<'a, T, U> Iterator for PredIter<'a, T, U> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        (&mut self.arcs).zip(&mut self.nodes).filter(|(a,_)| a.len() > 0).map(|(_,(i, _))| i).next()
    }
}


#[macro_export]
macro_rules! graph {
    ($n:expr, $($src:expr => $dst:expr),*) => {
        {
            let mut g = Graph::with_capacity($n);
            for _ in 0..$n {
                g.new_node(());
            }
            $(
                for arc in &$dst {
                    g.new_arc($src, *arc, ());
                }
            )*
            g
        }
    };
    ($n:expr, $($src:expr => $dst:expr,)*) => {
        graph!($n, $($src => $dst),*)
    }
}

#[macro_export]
macro_rules! graph_arc {
    ($n:expr, $($src:expr => $dst:expr),*) => {
        {
            let mut g = Graph::with_capacity($n);
            for _ in 0..$n {
                g.new_node(());
            }
            $(
                for (arc, v) in &$dst {
                    g.new_arc($src, *arc, *v);
                }
            )*
            g
        }
    };
    ($n:expr, $($src:expr => $dst:expr,)*) => {
        graph!($n, $($src => $dst),*)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_macro() {
        use std::fs::File;
        let g = graph_arc!(4, 0 => [(1, 12), (2, 3)], 1 => [(2, 5)]);
        g.map(|i, _| ('A'..='Z').nth(i).unwrap(), |_, v| *v)
            .to_dot(File::create("out.dot").unwrap()).unwrap();
    }

    #[test]
    fn to_dot() {
        use std::fs::File;
        let mut g = Graph::new();
        g.new_node(());
        g.new_node(());
        g.new_node(());
        g.new_node(());
        g.new_arc(0, 1, 12);
        g.new_arc(0, 2, 1);
        g.new_arc(1, 2, 6);
        let g = g.map(|i, _| ('A'..='Z').nth(i).unwrap(), |_, v| *v);
        g.to_dot(File::create("out.dot").unwrap()).unwrap();
    }
}
